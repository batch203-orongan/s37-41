
const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// Allows our backend application to be available with our frontend application.
	// Cross Origin Resource Sharing
const cors = require("cors");

const app = express();

// mongoDb connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.ahwkt4x.mongodb.net/b203_bookingAPI?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "connection error"));

	db.once("open", () => console.log("We're connected to the cloud database."));


// middlewares
// Allow our resources to access our backend application
app.use(cors());
app.use(express.json());

// "/courses" string will be included for all course routes inside the "courseRoutes" file
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


// this syntax will allow flexibility when using application locally or as hosted application
const port = process.env.PORT || 4000;

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})