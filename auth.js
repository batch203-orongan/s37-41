const jwt = require("jsonwebtoken");

// Use in the algorithm for encrypting our data which makes it difficult to decode the information without he defined secret key.
const secret = "CourseBookingAPI";

// [SECTION] JSON Web Tokens

// Token creation
/*
	Analogy:
		Pack the gift provided with a provided lock, which can only be open using the secret code as the key.
*/

// The "user" parameter will contain the values of the user upon login.
module.exports.createAccessToken = (user) => {

	console.log(user);

	// payload of the JWT
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's "sign method".
	// Syntax:
		// jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])

	return jwt.sign(data, secret, {});
}

// Token Verification
/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/

// Middleware function
module.exports.verify = (req, res, next) => {

	// The token is retrieve from the request header.
	let token = req.headers.authorization;

	// console.log(token);
	// If token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorozation headers.
	if(token !== undefined){
		//res.send({message: "Token recieved!"})

		// The token sent is a type of "Bearer" which whn recieved contains the "Bearer" as a prefix to the string. To remove the "Bearer" prefix we used the slice method to retrieve only the token.
		token = token.slice(7, token.length);

		console.log(token);

		// validate the "token" using the "verify" method to decrypt the token using the secret code
		// Syntax: jwt.verify(token,secretOrPrivateKey, [options/callBackFunction])

		return jwt.verify(token,secret, (err, data) =>{
			//If JWT is not valid
			if(err){
				return res.send({auth: "Invalid token!"})
			}
			// If JWT is valid
			else{
				// Allows application to proceed with the next middleware function/callback function in the route.
				next();
			}
		})

	}
	else{
		res.send({message: "Auth failed! No token provided!"})
	}
}

// Token decryption
/*
	-Analogy
		Open the gift and get the content
*/

module.exports.decode = (token) =>{
	if(token !== undefined){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				// If token is not valid
				return null;
			}
			else{
				// Decode method is used to obtain the information from JWT
				// Syntax: jwt.decode(token, [options]);
				// Returns a n object with access to the "payload" property which contains the user information stored when the token is generated
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else{
		// If token does not exist
		return null
	}
}