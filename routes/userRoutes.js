
const express = require("express");
const router = express.Router();

const userControllers = require("../controller/UserController");
const auth = require("../auth");

console.log(userControllers);

// Routes for checking email
router.post("/checkEmail", userControllers.checkEmailExists);

// Routes for user registration
router.post("/register", userControllers.registerUser);

// Route for user Authentication
router.post("/login", userControllers.loginUser);

// Route for details of a user
router.get("/details", auth.verify, userControllers.getProfile)

// Route for enrolling a user
router.post("/enroll", auth.verify, userControllers.enroll);

module.exports = router;