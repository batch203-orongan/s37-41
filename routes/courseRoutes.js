
const express = require("express");
const router = express.Router();
const courseControllers = require("../controller/courseControllers");
const auth = require("../auth");


console.log(courseControllers);

// Route for creating course
router.post("/",auth.verify, courseControllers.addCourse);

// Route for viewing all courses (admin only)
router.get("/all", auth.verify, courseControllers.getAllCourses);

// Route for viewing all active courses (all users)
router.get("/", courseControllers.getAllActive);

// Routes for viewing a specific course
router.get("/:courseId",courseControllers.getCourse);

// Routes for updating a course
router.put("/:courseId", auth.verify, courseControllers.updateCourse);

// Routes for archiving a course
router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);

module.exports = router;
