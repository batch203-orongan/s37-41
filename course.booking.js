/*

	Scenario:
		A course booking system application where a user can enroll into a course.

	Type: Course Booking System (Web App)
	Description: A course booking system application where a user can enroll into a course.

	Features:
		- User Login (User Authentication)
		- User Registration

		Customer/Authenticated Users:
			- Enroll Course
		
		Admin Users:
			- Add Course
			- Update Course
			- Archive/Unarchive a course (soft delete/reactivate the course)
			- View Courses (All courses active/inactive)
			- View/Manage User Accounts*

		All Users (guest, customers, admin)
			- View a Specific Courses
			- View Courses (all active courses)
			- View User Profile

*/

	//Data Model fo the Booking System

	//Two-way Embedding
		user {

			id - unique identifier for the document,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments: [
				{

					id - document identifier,
					courseId - the unique identifier for the course,
					courseName - optional,
					isPaid,
					dateEnrolled
				}
			]

		}


		course {

			id - unique for the document (auto generated)
			name,
			description,
			price,
			slots,
			isActive,
			createdOn, 
			enrollees: [

				{
					id - document identifier (auto generated),
					userId,
					email,
					isPaid,
					dateEnrolled
				}
			]

		}

		// With Referencing

		user {

			id - unique identifier for the document,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin

		}

		course {

			id - unique for the document
			name,
			description,
			price,
			slots,
			isActive

		}

		enrollment {

			id - document identifier,
			userId - the unique identifier for the user,
			courseId - the unique identifier for the course,
			courseName - optional,
			isPaid,
			dateEnrolled

		}
*/
