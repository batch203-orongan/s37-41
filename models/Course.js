/*
	course {

			id - unique for the document (auto generated)
			name,
			description,
			price,
			slots,
			isActive,
			createdOn, 
			enrollees: [

				{
					id - document identifier (auto generated),
					userId,
					email,
					isPaid,
					dateEnrolled
				}
			]

		}
*/

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	slots: {
		type: Number,
		required: [true, "Slots is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			email:{
				type: String,
				required: [true, "Email is required"]
			},
			isPaid: {
				type: Boolean,
				default: true
			},
			dateEnrolled:{
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("Course", courseSchema)